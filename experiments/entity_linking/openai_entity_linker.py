# SPDX-FileCopyrightText: 2024 Georg-August-Universität Göttingen
#
# SPDX-License-Identifier: CC0-1.0

import importlib
import os
from typing import Any
from openai import OpenAI

from spacy.language import Language
from spacy.pipeline.entity_linker import EntityLinker
from spacy.tokens import Doc, Token


@Language.factory("openai_entity_linker")
def openai_entity_linker(nlp: Language, name: str, openai_api_key: str = None) -> Any:
    """Spacy component implementation.
    
    Args:
        nlp: Spacy language object.
        name: Name of the component.
    
    Returns:
        `OpenaiEntityLinker` object.
    """
    return OpenaiEntityLinker(nlp, openai_api_key)

class OpenaiEntityLinker():
    """The class `OpenaiEntityLinker`."""

    def __init__(self, nlp: Language, openai_api_key: str = None):

        if "OPENAI_API_KEY" in os.environ and openai_api_key is None:
            openai_api_key = os.environ["OPENAI_API_KEY"]
        elif openai_api_key is None:
            raise ValueError("OpenAI API key is required")

        self.openai_api_key = openai_api_key

    def __call__(self, doc: Doc) -> Doc:
        """Process the document.
        
        Args:
            doc: Spacy document object.
        
        Returns:
            Spacy document object.
        """

        client = OpenAI(api_key=self.openai_api_key)
        completion = client.chat.completions.create(
        model="gpt-4",
        messages=[
            {"role": "system", "content": "You are a poetic assistant, skilled in explaining complex programming concepts with creative flair."},
            {"role": "user", "content": "Compose a poem that explains the concept of recursion in programming."}
        ]
        )

        return doc

