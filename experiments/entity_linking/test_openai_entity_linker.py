import monapipe.model

from openai_entity_linker import OpenaiEntityLinker
from spacy.language import Language


text = "Johann Wolfgang Gothe wohnte in Weimar und schrieb den Faust."
nlp = monapipe.model.load()
doc = nlp(text)

OpenaiEntityLinker(nlp=nlp).__call__(doc)
