chromadb #0.4.22
GPT4All #2.2.0
ollama #0.1.6
langchain #0.1.5
pysqlite3-binary
wikipedia #1.4.0

#dev
black==23.3.0
ipykernel
commitizen==3.13.0
pylint==3.0.3
pylintfileheader==1.0.0