# Individual Experiments

- Prerequisites: 

    Python >=3.8

- Set-up a virtual environment:

```sh
python3 -m venv env
source env/bin/activate
```

- Upgrade `pip`:

```sh
pip install --upgrade pip
```

- Install requirements and requirements.dev:

```sh
pip install -r requirements.txt -r requirements.dev.txt
```